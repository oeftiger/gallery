# Beam Dynamics

This is a gallery of basic example notebooks about beam dynamics. The library used is [PyHEADTAIL][pyheadtail]: click on the images to inspect the underlying document, open in SWAN the single notebooks or the full git repository!

[PyHEADTAIL][pyheadtail] is

- a python library to numerically simulate particle beam dynamics
- based on macro-particles to model collective effects of charged beams
- developed and maintained in the BE-ABP group at CERN

[PyHEADTAIL][pyheadtail] is used at CERN and other labs to study a wide range of collective effects in circular accelerators. It models beam interactions with wake fields and impedances, electron clouds, self-consistent space charge or realistic feedback systems, for instance. In particular, at CERN we use [PyHEADTAIL][pyheadtail] to study instabilities in all of the CERN synchrotrons in order to understand performance limiting mechanisms and to devise adequate mitigation schemes.

[<img class="open_in_swan" data-path="beams_dynamics" alt="Open this Gallery in SWAN" src="https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png">][gallery_url]

* [Quick Start Tutorial](beams_dynamics/index.ipynb)
* [Transverse Gaussian Space Charge](beams_dynamics/TransverseGaussianSpaceCharge.ipynb)
* [PS Triple Bunch Splitting](beams_dynamics/PS-TripleBunchSplitting.ipynb)
* [RF Bucket Matching](beams_dynamics/RFBucket_Matching.ipynb)
* [Transverse Tracking](beams_dynamics/TransverseTrackingTest.ipynb)
* [Detuners](beams_dynamics/DetunersTest.ipynb)
* [Transverse Mode Coupling Instability](beams_dynamics/TMCI_2particle_model.ipynb)


[pyheadtail]:https://github.com/PyCOMPLETE/PyHEADTAIL/wiki
[gallery_url]:https://cern.ch/swanserver/cgi-bin/go/?projurl=https://github.com/PyCOMPLETE/PyHEADTAIL-playground.git